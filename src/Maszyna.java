import java.util.Random;


public class Maszyna {

    private String[] tablicaDoLosowania = {"|@|", "|$|", "|#|", "|&|", "|*|", "|@|", "|$|", "|#|", "|&|", "|*|", "|7|"};
    private String[][] tablicaWynikow;
    private String okienkoSrodek;
    private String okienkoPoprzedni;
    private String okienkoNastepny;
    private int indexSrodek;
    private int indexPoprzedni;
    private int indexNastepny;
    private int dl = tablicaDoLosowania.length - 1;

    Random rand = new Random();


    public Maszyna() {
        generujEkran();

    }


    public String[][] generujEkran() {
        tablicaWynikow = new String[3][3];
        for (int i = 0; i < tablicaWynikow.length; i++) {
            for (int j = 0; j < tablicaWynikow[i].length; j++) {
                tablicaWynikow[i][j] = "nic";
            }
        }
        for (int i = 0; i < tablicaWynikow.length; i++) {
            for (int j = 0; j < tablicaWynikow[i].length; j++) {
                indexSrodek = rand.nextInt(dl);
                if (indexSrodek == dl) {
                    indexNastepny = 0;
                    indexPoprzedni = indexSrodek - 1;
                } else if (indexSrodek == 0) {
                    indexNastepny = indexSrodek + 1;
                    indexPoprzedni = dl;
                } else {
                    indexPoprzedni = indexSrodek - 1;
                    indexNastepny = indexSrodek + 1;
                }
                okienkoSrodek = tablicaDoLosowania[indexSrodek];
                okienkoPoprzedni = tablicaDoLosowania[indexPoprzedni];
                okienkoNastepny = tablicaDoLosowania[indexNastepny];
                if (i == 1) {
                    tablicaWynikow[i][j] = okienkoSrodek;
                    tablicaWynikow[i - 1][j] = okienkoPoprzedni;
                    tablicaWynikow[i + 1][j] = okienkoNastepny;
                }
            }
        }
        return tablicaWynikow;
    }


    public String[][] getTablicaWynikow() {
        return tablicaWynikow;
    }
}
