import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Gracz {

    private String imie;
    private int coins = 0;
    private boolean czyKoniec = true;
    private int wklad;
    private Maszyna maszynka;
    private String poRundzie;
    private String start;

    private boolean wygranaPozioma;
    private boolean wygranaSkosna1;
    private boolean wygranaSkosna2;

    public Gracz(String imie) {
        this.imie = imie;
    }


    public void zapiszIloscMonet() throws FileNotFoundException {
        File file = new File("MyCoins");
        PrintWriter save = new PrintWriter(file);
        save.println(coins);
        save.close();
    }

    public void wczytajIloScMonet() throws FileNotFoundException {
        File file = new File("MyCoins");
        Scanner load = new Scanner(file);
        String temp = load.nextLine();
        coins = Integer.valueOf(temp);
    }


    public void graj() throws FileNotFoundException {
        System.out.println("(Zobaczysz ten wstęp zawsze kiedy rozpoczniesz grę)");
        System.out.println("----------Wstęp----------");
        System.out.println("Cześć " + imie + " Witaj w grze jednoręki bandyta :)");
        System.out.println("Otrzymasz 300 coinsów jeśli to Twoja pierwsza gra lub kiedy wydasz wszystkie");
        System.out.println("Wygrywasz kiedy w środkowej poziomej lini lub po skosie znajdą się 3 takie same symbole");
        System.out.println("Trzy |@|,|$|,|&| lub |#| pomnożą Twój wkład przez 10");
        System.out.println("Zaś |*| przez 50 a |7| przez 100");
        System.out.println("-------------------------");

        Scanner in = new Scanner(System.in);
        Scanner pyt = new Scanner(System.in);
        Scanner pyt2 = new Scanner(System.in);
        System.out.println("Czy to Twoja pierwsza gra? (tak/nie)");
        start = pyt2.nextLine();
        if (start.equals("tak")) {
            System.out.println("Otrzymujesz 300 coinsów na start");
            coins = 300;
        } else {
            wczytajIloScMonet();
            if (coins == 0) {
                System.out.println("Wygląda na to że przegrałeś wszystkie swoje monety, otzymujesz 300");
                coins = 300;
            }
        }
        while (czyKoniec) {
            System.out.println("Aktualnie posiadasz " + coins + " monet");

            System.out.println("Zaczynamy runde? (tak/nie)");
            poRundzie = pyt.nextLine();
            if (poRundzie.equals("nie")) {
                System.out.println("Koniec");
                zapiszIloscMonet();
                break;
            }
            System.out.println("Zaczynamy!");

            System.out.println("Ile monet chcesz postawić?");
            wklad = in.nextInt();
            System.out.println("Postawiłeś " + wklad + "monet");
            coins = coins - wklad;
            maszynka = new Maszyna();

            wygranaPozioma = (maszynka.getTablicaWynikow()[1][0].equals(maszynka.getTablicaWynikow()[1][1]) && (maszynka.getTablicaWynikow()[1][1].equals(maszynka.getTablicaWynikow()[1][2])));
            wygranaSkosna1 = (maszynka.getTablicaWynikow()[2][0].equals(maszynka.getTablicaWynikow()[1][1]) && (maszynka.getTablicaWynikow()[1][1].equals(maszynka.getTablicaWynikow()[0][2])));
            wygranaSkosna2 = (maszynka.getTablicaWynikow()[0][0].equals(maszynka.getTablicaWynikow()[1][1]) && (maszynka.getTablicaWynikow()[1][1].equals(maszynka.getTablicaWynikow()[2][2])));

            System.out.println("Tablica wyników:");
            System.out.println(maszynka.getTablicaWynikow()[0][0] + "  " + maszynka.getTablicaWynikow()[0][1] + "  " + maszynka.getTablicaWynikow()[0][2]);
            System.out.println("");
            System.out.println(maszynka.getTablicaWynikow()[1][0] + "  " + maszynka.getTablicaWynikow()[1][1] + "  " + maszynka.getTablicaWynikow()[1][2]);
            System.out.println("");
            System.out.println(maszynka.getTablicaWynikow()[2][0] + "  " + maszynka.getTablicaWynikow()[2][1] + "  " + maszynka.getTablicaWynikow()[2][2]);
            System.out.println("");
            System.out.println("");
            if (wygranaPozioma || wygranaSkosna1 || wygranaSkosna2) {
                if (maszynka.getTablicaWynikow()[1][1].equals("|&|") || maszynka.getTablicaWynikow()[1][1].equals("|$|") || maszynka.getTablicaWynikow()[1][1].equals("|#|") || maszynka.getTablicaWynikow()[1][1].equals("|@|")) {
                    coins = coins + wklad * 10;
                    System.out.println("Wynik: 3x " + maszynka.getTablicaWynikow()[1][1]);
                    System.out.println("Brawo, wygrałeś " + wklad * 10 + " monet!");
                    System.out.println("Aktualnie posiadasz " + coins + " monet.");
                    System.out.println("Czy chcesz zakonczyć grę, czy gramy dalej? (gram/koniec)");
                    poRundzie = pyt.nextLine();
                    if (poRundzie.equals("gram")) {
                        czyKoniec = true;
                    } else if (poRundzie.equals("koniec")) {
                        System.out.println("Gra zakończona, masz teraz " + coins + " monet");
                        zapiszIloscMonet();
                        czyKoniec = false;
                    } else {
                        System.out.println("Nie rozumiem.. wracamy na początek gry");
                        czyKoniec = true;
                    }

                }
                if (maszynka.getTablicaWynikow()[1][1].equals("|*|")) {
                    coins = coins + wklad * 50;
                    System.out.println("Wynik: 3x " + maszynka.getTablicaWynikow()[1][1]);
                    System.out.println("Brawo, wygrałeś " + wklad * 50 + " monet!");
                    System.out.println("Aktualnie posiadasz " + coins + " monet.");
                    System.out.println("Czy chcesz zakonczyć grę, czy gramy dalej? (gram/koniec)");
                    poRundzie = pyt.nextLine();
                    if (poRundzie.equals("gram")) {
                        czyKoniec = true;
                    } else if (poRundzie.equals("koniec")) {
                        System.out.println("Gra zakończona, masz teraz " + coins + " monet");
                        zapiszIloscMonet();
                        czyKoniec = false;
                    } else {
                        System.out.println("Nie rozumiem.. wracamy na początek gry");
                        czyKoniec = true;
                    }
                }
                if (maszynka.getTablicaWynikow()[1][1].equals("|7|")) {
                    coins = coins + wklad * 100;
                    System.out.println("Wynik: 3x " + maszynka.getTablicaWynikow()[1][1]);
                    System.out.println("Brawo, wygrałeś " + wklad * 100 + " monet!");
                    System.out.println("Aktualnie posiadasz " + coins + " monet.");
                    System.out.println("Czy chcesz zakonczyć grę, czy gramy dalej? (gram/koniec)");
                    poRundzie = pyt.nextLine();
                    if (poRundzie.equals("gram")) {
                        czyKoniec = true;
                    } else if (poRundzie.equals("koniec")) {
                        System.out.println("Gra zakończona, masz teraz " + coins + " monet");
                        zapiszIloscMonet();
                        czyKoniec = false;
                    } else {
                        System.out.println("Nie rozumiem.. wracamy na początek gry");
                        czyKoniec = true;
                    }

                }
            } else {
                System.out.println("Niestety - przegrałeś! :<");
                System.out.println("Aktualnie posiadasz " + coins + " monet.");
                if (coins <= 0) {
                    System.out.println("Nie masz już monet, koniec gry :(");
                    zapiszIloscMonet();
                    break;
                }
                System.out.println("Czy chcesz zakonczyć grę, czy gramy dalej? (gram/koniec)");
                poRundzie = pyt.nextLine();
                if (poRundzie.equals("gram")) {
                    czyKoniec = true;
                } else if (poRundzie.equals("koniec")) {
                    System.out.println("Gra zakończona, masz teraz " + coins + " monet");
                    zapiszIloscMonet();
                    czyKoniec = false;
                } else {
                    System.out.println("Nie rozumiem.. wracamy na początek gry");
                    czyKoniec = true;
                }
            }

        }


    }

    public void setCoins(int coins) {
        this.coins = coins;
    }


    public int getCoins() {
        return coins;
    }
}